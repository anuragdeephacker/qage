<?php

// FormController.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $data = request()->validate(
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'phone' => ['required', 'string', 'min:10', 'max:13'],
                'dob' => 'date_format:Y-m-d|before:today|nullable',
                'city' => ['required', 'string'],
            ]
        );

        auth()->user()->update($data);

        return redirect('/user/show');
    }

    public function show()
    {
        $user = auth()->user();

        return view('show', compact('user'));
    }
}
