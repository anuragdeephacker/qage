@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-dark text-white">{{ __('User Details') }}</div>
                <div class="card-body bg1">

<div class="container">
  <table class="table table-hover">
    <tbody>

    <tr>
        <th>Name</th>
        <td>{{ $user->name}}</td>
      </tr>
      <tr>
        <th>Email</th>
        <td>{{ $user->email}}</td>
      </tr>
      <tr>
        <th>Phone</th>
        <td>{{ $user->phone}}</td>
      </tr>
      <tr>
        <th>Current City</th>
        <td>{{ $user->city}}</td>
      </tr>
      <tr>
        <th>Date of birth</th>
        <td>{{ $user->dob}}</td>
      </tr>
    </tbody>
  </table>
</div>





                </div>
            </div>
        </div>
    </div>
</div>
@endsection
