<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Project</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

           <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
   
   <!-- Styles -->
   <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color:#56078a ;
                color: #fff;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .btn{
                border: #fff;
                border-style: solid;
                padding: 15px;
                margin: 5px;
            }

            .btn:hover{
                border: #E74C3C;
                border-style: solid;
                padding: 15px;
                margin: 5px;
            }


            .links > a {
                color: #fff;
                padding: 0 25px;
                font-size: 22px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .links > a:hover {
                color: #fff;
                padding: 0 25px;
                font-size: 22px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">


            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a class="btn" href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a class="btn" href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif




            <div class="card text-white bg-primary">
  <div class="card-header text-white bg-dark">
    Features of application
  </div>


    <div class="list-group bg1">
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
    <div class="d-flex w-100 justify-content-between bg1">
      <h5 class="mb-1">1. Sign Up</h5>
    </div>
    <p class="mb-1">With User (Name, Email, Phone, DOB, Password, Confirm Password) - With Validation of Form.</p>

  </a>
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
    <div class="d-flex w-100 justify-content-between bg1">
      <h5 class="mb-1">2. Log In User</h5>
    </div>
    <p class="mb-1">Auth with Email or Phone and Password.</p>
  </a>


  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
    <div class="d-flex w-100 justify-content-between bg1">
      <h5 class="mb-1">3. After Login</h5>
    </div>
    <p class="mb-1">It will Redirect to Edit Profile Detail Form, where User can update details.</p>
  </a>



  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
    <div class="d-flex w-100 justify-content-between bg1">
      <h5 class="mb-1">4. Edit Profile Details</h5>
    </div>
    <p class="mb-1">Fiels for Updatation (Name, Email, Phone, Current City, DOB) -> User should be able to update its profile details
    </p><p> After Successful Updation it will redirect user to Success Page and Show the user details</p>
  </a>
</div>


  </div>
</div>

  


    </body>
</html>
